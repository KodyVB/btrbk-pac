#!/bin/sh

PATH=/usr/bin:/bin

[ ! -f scripts/btrbk_pac_log ] && printf "Logging program not available.\n" && exit 1
[ ! -f scripts/btrbk_pac_log_script ] && printf "Logging program script not available.\n" && exit 1
[ ! -f hooks/00-btrbk-pre.hook ] && printf "Pre-pacman hook not available.\n" && exit 1
[ ! -f hooks/zx-btrbk-post.hook ] && printf "Post-pacman hook not available.\n" && exit 1
[ ! -f etc/btrbk_logger.conf.example ] && printf "Example logger config not available.\n" && exit 1

cp -v scripts/btrbk_pac_log /usr/local/bin/btrbk_pac_log
cp -v scripts/btrbk_pac_log_script /usr/share/libalpm/scripts/btrbk_pac_log_script
cp -v hooks/00-btrbk-pre.hook /usr/share/libalpm/hooks/00-btrbk-pre.hook
cp -v hooks/zx-btrbk-post.hook /usr/share/libalpm/hooks/zx-btrbk-post.hook
[ ! -d /etc/btrbk_logger ] && mkdir /etc/btrbk_logger
cp -v etc/btrbk_logger.conf.example /etc/btrbk_logger/btrbk_logger.conf.example

exit 0

